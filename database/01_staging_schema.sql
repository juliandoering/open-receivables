-- this sql files inits the database for all source files/ data

-- a schema to handle all staging tables
CREATE SCHEMA IF NOT EXISTS staging;

CREATE TABLE IF NOT EXISTS staging.excel_563 (
    id SERIAL,
    d_k VARCHAR,
    account_number INT,
    matchcode VARCHAR,
    company VARCHAR,
    type VARCHAR, -- i=inventory, p=purchase contract, s=sales contract
    purchase_contract INT,
    sales_contract INT,
    voucher_number INT,
    origin_code INT,
    origin VARCHAR,
    shippment_month INT,
    inventory_date INT, -- or date?
    terms_of_delivery VARCHAR,
    shipment_del_text VARCHAR,
    type_of_loading VARCHAR,
    payment_date INT, -- or date
    payment_text VARCHAR,
    uninv_bags INT,
    open_value_fc BIGINT,
    open_value_lc BIGINT, -- maybe int
    open_value_rc BIGINT, -- maybe int
    open_value_internal BIGINT, -- maybe int
    open_hermes_values BIGINT, -- maybe int
    lc VARCHAR,
    fc VARCHAR,
    rc VARCHAR,
    currency_internal_limit VARCHAR,
    currency_hermes_limit VARCHAR,
    ol_voucher_date INT, --or date
    value_date INT, -- or date
    ol_information VARCHAR,
    ol_inv_value_in_lc BIGINT,
    ol_inv_value_in_fc BIGINT,
    average_months_shipment_inventory INT,
    internal_limit INT,
    current_internal_limit_1 INT, -- maybe bigint
    report_internal_y_n VARCHAR,
    remarks_internal_limit VARCHAR,
    hermes_limit INT, -- maybe bigint
    current_hermes_limit_1 INT, -- maybe bigint
    report_hermes_z_n VARCHAR,
    remarks_hermes_limit VARCHAR,
    general_limit_remarks VARCHAR,
    report_remarks VARCHAR,
    days_payment_terms INT,
    days_overdue INT,
    value_overdue_01_30 INT,
    value_overdue_31_60 INT,
    value_overdue_61_90 INT,
    value_overdue_91_120 INT,
    value_overdue_bigger_120 INT
);

-- excel file 678
CREATE TABLE IF NOT EXISTS staging.excel_678 (
    id SERIAL,
    company VARCHAR,
    account VARCHAR,
    account_number INT,
    matchcode VARCHAR,
    staats_kennzeichen INT,
    contract_number INT,
    line_type INT,
    contract_reference INT,
    boerse VARCHAR,
    verschiffung_termin VARCHAR, -- or int?
    boersen_term VARCHAR, -- or int?
    lieferkonditionen VARCHAR,
    verschiffung_text VARCHAR,
    verlade_art INT,
    text_info_1 VARCHAR,
    contract_date INT, -- or varchar or date?
    ktr_sack INT, -- what is ktr?
    lot_u  VARCHAR,
    lot_o VARCHAR,
    lot_p VARCHAR,
    lot_a VARCHAR,
    lot_s VARCHAR,
    origin INT,
    quality_code INT,
    crop INT,
    bags INT,
    hints VARCHAR,
    cost_value BIGINT,
    market_value BIGINT,
    profit BIGINT,
    a VARCHAR,
    b VARCHAR,
    c VARCHAR,
    d VARCHAR,
    e VARCHAR,
    f VARCHAR,
    g VARCHAR,
    h VARCHAR,
    j VARCHAR,
    ktr_text_1 VARCHAR,
    ktr_text_2 VARCHAR,
    info_text_1 VARCHAR,
    info_text_2 VARCHAR,
    info_text_3 VARCHAR,
    info_text_4 VARCHAR,
    info_text_5 VARCHAR,
    info_text_6 VARCHAR,
    info_text_7 VARCHAR,
    info_text_8 VARCHAR,
    info_text_9 VARCHAR,
    info_text_10 VARCHAR,
    status_date DATE -- this field is not in the excel but in the access database
);

-- excel file 865
CREATE TABLE IF NOT EXISTS staging.excel_865 (
    id SERIAL,
    company VARCHAR,
    purchase_contract INT,
    sales_contract INT,
    contractor VARCHAR,
    matchcode VARCHAR,
    debitor_kreditor VARCHAR,
    account_number INT,
    reference INT,
    origin VARCHAR,
    condition_code INT,
    condition VARCHAR,
    shipment_date INT, -- or date
    terms_of_delivery VARCHAR,
    shipment VARCHAR,
    type_of_loading VARCHAR,
    payment_date INT, -- or date or varchar
    payment VARCHAR,
    contract_bags INT,
    inventory_bags INT,
    difference_bags INT,
    contract_price BIGINT,
    contract_currency_unit VARCHAR,
    differential BIGINT,
    fix_exchangerate VARCHAR,
    fix_month INT, -- or date or varchar
    amount_fc BIGINT,
    amount_rc BIGINT,
    currency_rc VARCHAR,
    origin_number INT,
    quality_number INT,
    crop INT,
    quality_group INT,
    sort_1 VARCHAR,
    sort_2 VARCHAR,
    sort_3 VARCHAR,
    status_date DATE -- this field is not in the excel but in the access database
);

-- this is inside the pivot excel
CREATE TABLE IF NOT EXISTS staging.mapping_origin (
    id SERIAL,
    origin VARCHAR,
    company VARCHAR,
    origin_number INT,
    quality_number INT
);

-- excel file mapping grading
CREATE TABLE IF NOT EXISTS staging.mapping_grading (
  id SERIAL,
  name_policyholder VARCHAR,
  company_abbreviation VARCHAR
);

-- excel file mapping 563
CREATE TABLE IF NOT EXISTS staging.mapping_563 (
    id SERIAL,
    comtras_company VARCHAR,
    mapping_1 VARCHAR,
    mapping_2 VARCHAR
);
